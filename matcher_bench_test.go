package fastregexp_test

import (
	"bytes"
	"regexp"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/vulnerability-research/foss/go-fastregexp"
)

func Benchmark(b *testing.B) {
	benchmarks := []struct {
		name  string
		regex string
		match string
	}{
		{"literal_prefix", "glpat-[0-9a-zA-Z_\\-]{20}", "glpat-00000000000000000000"},
		{"no_prefix", ".glpat-[0-9a-zA-Z_\\-]{20}", "0glpat-00000000000000000000"},
		{"case_folded_prefix", "(?i)glpat-[0-9a-zA-Z_\\-]{20}", "glpat-00000000000000000000"},
	}

	lengths := []int{
		100,
		1000,
		10000,
		100000,
		1000000,
	}

	for _, bm := range benchmarks {
		matcher, err := fastregexp.Compile(bm.regex)
		if err != nil {
			b.Fatal(err)
		}
		if !matcher.Optimized() {
			b.Fatal("not optimized")
		}

		re, err := regexp.Compile(bm.regex)
		if err != nil {
			b.Fatal(err)
		}

		if !bytes.Equal(re.Find([]byte(bm.match)), []byte(bm.match)) {
			b.Fatal("no match")
		}

		for i := 0; i < 4; i++ {
			for _, length := range lengths {
				input := generateInput(length, bm.match)

				b.Run("baseline_"+strconv.Itoa(length)+"_"+bm.name, func(b *testing.B) {
					for i := 0; i < b.N; i++ {
						result := re.Find(input)

						if len(result) != len(bm.match) {
							b.Fail()
						}
					}
				})

				b.Run("optimized_"+strconv.Itoa(length)+"_"+bm.name, func(b *testing.B) {
					for i := 0; i < b.N; i++ {
						result := matcher.Find(input)

						if len(result) != len(bm.match) {
							b.Fail()
						}
					}
				})
			}
		}
	}
}

func generateInput(n int, match string) []byte {
	return []byte(strings.Repeat("0", n) + match)
}
