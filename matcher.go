package fastregexp

import (
	"bytes"
	"fmt"
	"regexp"

	"github.com/pkg/errors"
)

const DefaultWindowLength = 256

type Matcher struct {
	lits       [][]byte
	foldedLits [][]byte
	re         *regexp.Regexp
}

func Compile(expr string) (*Matcher, error) {
	re, err := regexp.Compile(expr)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to compile regex: %s", expr)
	}

	lits, foldedLits, err := FindLiterals(expr)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to find literal in regex: %s", expr)
	}

	m := &Matcher{
		lits:       make([][]byte, 0, len(lits)),
		foldedLits: make([][]byte, 0, len(foldedLits)),
		re:         re,
	}
	for _, lit := range lits {
		m.lits = append(m.lits, []byte(lit))
	}
	for _, foldedLit := range foldedLits {
		m.foldedLits = append(m.foldedLits, []byte(foldedLit))
	}

	return m, nil
}

func (m *Matcher) Optimized() bool {
	return len(m.lits) != 0 || len(m.foldedLits) != 0
}

func (m *Matcher) Find(input []byte) []byte {
	return m.FindWindow(input, DefaultWindowLength)
}

func (m *Matcher) FindWindow(input []byte, windowLength int) []byte {
	if !m.Optimized() {
		return m.re.Find(input)
	}

	for _, lit := range m.lits {
		result := m.findLiteral(lit, input, input, windowLength)
		if result != nil {
			return result
		}
	}

	if len(m.foldedLits) == 0 {
		return nil
	}

	upper := bytes.ToUpper(input)

	for _, foldedLit := range m.foldedLits {
		result := m.findLiteral(foldedLit, upper, input, windowLength)
		if result != nil {
			return result
		}
	}

	return nil
}

func (m *Matcher) String() string {
	if m.Optimized() {
		return fmt.Sprintf("fastregexp (optimized): %s", m.re.String())
	} else {
		return fmt.Sprintf("fastregexp (unoptimized): %s", m.re.String())
	}
}

func (m *Matcher) findLiteral(literal []byte, normalizedInput []byte, originalInput []byte, windowLength int) []byte {
	offset := 0

	for s := normalizedInput; len(s) > 0; {
		i := bytes.Index(s, literal)
		if i == -1 {
			break
		}
		offset += i

		window := extractWindow(originalInput, offset, offset+len(literal), windowLength)
		result := m.re.Find(window)
		if result != nil {
			return result
		}

		offset += len(literal)
		s = s[i+len(literal):]
	}

	return nil
}

func extractWindow(input []byte, start, end, windowLength int) []byte {
	startOffset := start - windowLength
	if startOffset < 0 {
		startOffset = 0
	}

	endOffset := end + windowLength
	if endOffset > len(input) {
		endOffset = len(input)
	}

	return input[startOffset:endOffset]
}
