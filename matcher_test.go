package fastregexp_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/vulnerability-research/foss/go-fastregexp"
)

func TestFind(t *testing.T) {
	tests := []struct {
		regex         string
		input         string
		expectedMatch string
		window        int
	}{
		// Unoptimized cases always run against full input
		{"^.*$", "abc", "abc", -1},
		{"^.*$", strings.Repeat("0", 10000), strings.Repeat("0", 10000), -1},
		{"^.*$", strings.Repeat("0", 10000), strings.Repeat("0", 10000), 50},

		// Optimized cases run against windowed input
		{"^a.*$", "a" + strings.Repeat("b", 10000), "a" + strings.Repeat("b", fastregexp.DefaultWindowLength), -1},
		{"^a.*$", "a" + strings.Repeat("b", 10000), "abb", 2},

		// Optimized case folding
		{"a(?i)B.*", "ab", "ab", -1},
		{"a(?i)B.*", "aB", "aB", -1},
		{"a(?i)B.*", "abc", "abc", -1},
		{"a(?i)B.*", "AB", "", -1},

		// Match beyond first literal occurrence
		{"abc.(?:foo|bar)", "abc" + strings.Repeat("b", 10000) + "abc_foo abc_bar", "abc_foo", -1},
		{"abc.(?:foo|bar)", "abc" + strings.Repeat("b", 10000) + "abc" + strings.Repeat("b", 10000) + "abc_foo", "abc_foo", -1},

		// Match all literals
		{"(?:foo|bar)", "foo", "foo", -1},
		{"(?:foo|bar)", "bar", "bar", -1},
	}

	for _, test := range tests {
		m, err := fastregexp.Compile(test.regex)
		require.NoError(t, err, "failed error test with regex %s", test.regex)

		var f []byte
		if test.window < 0 {
			f = m.Find([]byte(test.input))
		} else {
			f = m.FindWindow([]byte(test.input), test.window)
		}
		require.Equal(t, test.expectedMatch, string(f), "failed match test with regex %s", test.regex)
	}
}

func TestString(t *testing.T) {
	m, err := fastregexp.Compile(".*")
	require.NoError(t, err)
	require.Equal(t, "fastregexp (unoptimized): .*", m.String())

	m, err = fastregexp.Compile("abc")
	require.NoError(t, err)
	require.Equal(t, "fastregexp (optimized): abc", m.String())
}
