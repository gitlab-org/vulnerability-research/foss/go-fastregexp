go-fastregexp changelog

# v0.3.0
- Change package to `gitlab.com/gitlab-org/vulnerability-research/foss/go-fastregexp` (!5)

# v0.2.1
- Mention litregex status in `Matcher.String()` (!4)

# v0.2.0
- Change package to `gitlab.com/gitlab-org/secure/vulnerability-research/research/litregex` (!3)

# v0.1.1
- Add comments and rename variables (!1)

# v0.1.0
- Initial pre-release
