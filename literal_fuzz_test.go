//go:build go1.18
// +build go1.18

package fastregexp_test

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/vulnerability-research/foss/go-fastregexp"
)

func FuzzFindLiterals(f *testing.F) {
	// Seed corpus from https://pkg.go.dev/regexp/syntax

	// Single characters:
	f.Add(".")
	f.Add("[xyz]")
	f.Add("[^xyz]")
	f.Add("\\d")
	f.Add("\\D")
	f.Add("[[:alpha:]]")
	f.Add("[[:^alpha:]]")
	f.Add("\\pN")
	f.Add("\\p{Greek}")
	f.Add("\\PN")
	f.Add("\\P{Greek}")

	// Composites:
	f.Add("xy")
	f.Add("x|y")

	// Repetitions:
	f.Add("x*")
	f.Add("x+")
	f.Add("x?")
	f.Add("x{2,5}")
	f.Add("x{1,}")
	f.Add("x{4}")
	f.Add("x*?")
	f.Add("x+?")
	f.Add("x??")
	f.Add("x{3,8}?")
	f.Add("x{2,}?")
	f.Add("x{5}?")

	// Grouping:
	f.Add("(re)")
	f.Add("(?P<name>re)")
	f.Add("(?:re)")
	f.Add("(?i)")
	f.Add("(?m)")
	f.Add("(?s)")
	f.Add("(?U)")
	f.Add("(?imsU)")
	f.Add("(?-imsU)")
	f.Add("(?i:re)")

	// Empty strings:
	f.Add("^")
	f.Add("$")
	f.Add("\\A")
	f.Add("\\b")
	f.Add("\\B")
	f.Add("\\z")

	// Escape sequences:
	f.Add("\\n")
	f.Add("\\*")
	f.Add("\\Q...\\E")

	// Character class elements:
	f.Add("x")
	f.Add("A-Z")
	f.Add("\\d")

	// Named character classes as character class elements:
	f.Add("[\\d]")
	f.Add("[^\\d]")

	// Perl character classes (all ASCII-only):
	f.Add("\\d")
	f.Add("\\w")
	f.Add("\\W")

	// ASCII character classes:
	f.Add("[[:alnum:]]")
	f.Add("[[:graph:]]")

	f.Fuzz(func(t *testing.T, regex string) {
		_, err := regexp.Compile(regex)
		if err != nil {
			return
		}

		_, _, err = fastregexp.FindLiterals(regex)
		require.NoError(t, err)
	})
}
