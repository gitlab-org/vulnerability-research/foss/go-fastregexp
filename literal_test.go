package fastregexp

import (
	"math"
	"regexp/syntax"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFindLiterals(t *testing.T) {
	tests := []struct {
		regex              string
		expectedLits       []string
		expectedFoldedLits []string
	}{
		{"", nil, nil},
		{"test", []string{"test"}, nil},
		{"tes.t", []string{"tes"}, nil},
		{"a[bcd]ef", []string{"ef"}, nil},
		{"abc[de]fg", []string{"abc"}, nil},
		{"a[fbcdea]ef", []string{"ef"}, nil},
		{"a(test)?", []string{"a"}, nil},
		{"a|b|c", []string{"a", "b", "c"}, nil},

		// Perl features: Case folding
		{"(?i)", nil, nil},
		{"(?i)a", nil, []string{"A"}},
		{"(?i)A", nil, []string{"A"}},
		{"a.(?i)b", []string{"a"}, nil},
		{"(?i)a.(?-i)b", []string{"b"}, nil},
		{"ab.(?i)c", []string{"ab"}, nil},
		{"a.(?i)bc", nil, []string{"BC"}},
		{"a(?i)bc", nil, []string{"ABC"}},
		{"a|(?i)bc", []string{"a"}, []string{"BC"}},

		// Case folding should return ASCII-only literals
		{"a(?i)bc€", nil, []string{"ABC"}},
		{"a(?i)b€c", nil, []string{"AB"}},
		{"a(?i)€bc", nil, []string{"BC"}},
		{"a€(?i)bc", nil, []string{"BC"}},
		{"€a(?i)bc", nil, []string{"ABC"}},
	}

	for _, test := range tests {
		lits, foldedLits, err := FindLiterals(test.regex)

		require.NoError(t, err, test.regex, test)

		if len(test.expectedLits) > 0 {
			require.Equal(t, test.expectedLits, lits, test)
		} else {
			require.Empty(t, lits, test)
		}

		if len(test.expectedFoldedLits) > 0 {
			require.Equal(t, test.expectedFoldedLits, foldedLits, test)
		} else {
			require.Empty(t, foldedLits, test)
		}
	}
}

// Cases from https://gitlab.com/gitlab-org/security-products/dast-cwe-checks
func TestReallife(t *testing.T) {
	tests := []string{
		"(?:ORA)-\\d{5}:\\s.{1,50}",
		"(?:ADODB\\.(Field|Recordset|Connection))",
		"(?:\\[IBM\\].+SQL0\\d{3}N)",
		"DB2 (?:Driver|Error|ODBC)",
		"Microsoft OLE DB Provider for",
		"ODBC (?:DB2|Driver|Error|Microsoft Access|Oracle|SQL|SQL Server)",
		"Unclosed quotation mark before the character string",
		"Warning(:|</b>:)\\s+mysql_(query|connect|select_db)",
		"Warning(:|</b>:)\\s+pg_(query|connect)",
		"You have an error in your SQL syntax",
		"Server Error in '[^']+' Application",
		"PHP (?:Error|Parse error|Warning)",
		"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2 Final//EN\">\\s*<html>\\s*<head>\\s*<title>Index of /.*|<html>\\s*<head><title>Index of /.*|<html>\\s*<head><title>Index of /.*|(?s)<html><head><title>.* - /.*</title></head><body><H1>.* - /.*</H1><hr>.*<pre>.*\\[To Parent Directory\\].*",

		// Previously unhandled cases
		"(?:\\w{3,}\\.go:\\d+:\\s+[\\w\\s]{3,}|\\/[^\\/]+\\/[^\\/]+\\.go:\\d+)", // TODO Same literal ".go:" returned twice; make unique
		"(?:Exception\\s+of\\s+type.+[^t][^h][^r][^o][^w][^n]thrown|at\\s+(?:[A-Z]\\w+\\.)*\\w+\\([^j][^a][^v][^a][^)]*\\)|--- End of inner exception stack trace ---|\\[\\w+(?:\\s+\\(0x[\\w+\\d]+\\)|):\\s+Unable to load)",
		"(?:(\\s+at|\\s+&nbsp;+at)\\s[^:]+\\.js:\\d+:\\d+)",
		"(?:Exception in thread \"[^\"]+\" [^\\s]+|\\s+at\\s[^(]+\\((?:Unknown Source|Native Method))",
		"[^\\s:]+.rb:\\d+(?::in `|)",
		"(?:Traceback \\(most recent call last\\)|File \"[^\"]+\", line \\d+ in, [^\\s]+)",
	}

	for _, test := range tests {
		lits, foldedLits, err := FindLiterals(test)
		require.NoError(t, err, test)
		require.True(t, len(lits) > 0 || len(foldedLits) > 0, test)

		t.Log(test)
		t.Log(lits)
		t.Log(foldedLits)
		t.Log()
	}
}

func TestCompile(t *testing.T) {
	_, err := compile("*")
	require.Error(t, err)

	prog, err := compile("[a-b]{2}")
	require.NoError(t, err)

	as := 0
	bs := 0
	for _, inst := range prog.Inst {
		if inst.Op == syntax.InstRune1 && len(inst.Rune) == 1 {
			switch inst.Rune[0] {
			case 'a':
				as++
			case 'b':
				bs++
			}
		}
	}
	require.Equal(t, 2, as, prog)
	require.Equal(t, 2, bs, prog)
}

func TestUnrollRuneRanges(t *testing.T) {
	prog := &syntax.Prog{
		Inst: []syntax.Inst{
			{Op: syntax.InstRune, Out: 1, Rune: []rune{'a', 'c'}},
			{Op: syntax.InstAlt, Out: 0, Arg: 2},
			{Op: syntax.InstMatch},
		},
		Start:  0,
		NumCap: 42,
	}

	expected := &syntax.Prog{
		Inst: []syntax.Inst{
			{Op: syntax.InstRune, Out: 1, Rune: []rune{'a', 'c'}},
			{Op: syntax.InstAlt, Out: 3, Arg: 2},
			{Op: syntax.InstMatch},
			{Op: syntax.InstAlt, Out: 4, Arg: 7},
			{Op: syntax.InstAlt, Out: 5, Arg: 6},
			{Op: syntax.InstRune1, Out: 1, Rune: []rune{'a'}},
			{Op: syntax.InstRune1, Out: 1, Rune: []rune{'b'}},
			{Op: syntax.InstRune1, Out: 1, Rune: []rune{'c'}},
		},
		Start:  3,
		NumCap: 42,
	}

	actual := unrollRuneRanges(prog)
	require.Equal(t, expected, actual)
}

func TestEmitUnrolledInstructions(t *testing.T) {
	tests := []struct {
		counter       uint32
		out           uint32
		runes         []rune
		expectedInsts []syntax.Inst
	}{
		{0, 0, nil, []syntax.Inst{}},
		{1, 3, []rune{'a'}, []syntax.Inst{{Op: syntax.InstRune1, Out: 3, Rune: []rune{'a'}}}},
		{
			counter: 2,
			out:     7,
			runes:   []rune{'a', 'b', 'c'},
			expectedInsts: []syntax.Inst{
				{Op: syntax.InstAlt, Out: 3, Arg: 6},              // 2 (= counter)
				{Op: syntax.InstAlt, Out: 4, Arg: 5},              // 3
				{Op: syntax.InstRune1, Out: 7, Rune: []rune{'a'}}, // 4
				{Op: syntax.InstRune1, Out: 7, Rune: []rune{'b'}}, // 5
				{Op: syntax.InstRune1, Out: 7, Rune: []rune{'c'}}, // 6
			},
		},
	}

	for _, test := range tests {
		insts := emitUnrolledInstructions(test.counter, test.out, test.runes)
		require.Equal(t, test.expectedInsts, insts, test)
	}
}

func TestExtractRunesToUnroll(t *testing.T) {
	require.Equal(t, []rune{'0', '1', '2', '3', '4', '5', '6', '7'}, extractRunesToUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'0', '7'}}))
	require.Empty(t, extractRunesToUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'0', '8'}}))
	require.Equal(t, []rune{'a', 'b', 'c', 'd', '0', '1', '2', '3'}, extractRunesToUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'a', 'd', '0', '3'}}))

	require.Empty(t, extractRunesToUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{0, math.MaxInt32}}))
	require.Empty(t, extractRunesToUnroll(syntax.Inst{Op: syntax.InstRune1, Rune: []rune{'a'}}))
}

func TestShouldUnroll(t *testing.T) {
	require.Equal(t, true, shouldUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'0', '7'}}))
	require.Equal(t, false, shouldUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'0', '8'}}))
	require.Equal(t, true, shouldUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{'a', 'd', '0', '3'}}))

	require.Equal(t, false, shouldUnroll(syntax.Inst{Op: syntax.InstRune, Rune: []rune{0, math.MaxInt32}}))
	require.Equal(t, false, shouldUnroll(syntax.Inst{Op: syntax.InstRune1, Rune: []rune{'a'}}))
}

func TestRankLiterals(t *testing.T) {
	tests := []struct {
		traces             [][]uint32
		nofolds            map[uint32]string
		folds              map[uint32]string
		expectedLits       []string
		expectedFoldedLits []string
	}{
		{[][]uint32{}, map[uint32]string{}, map[uint32]string{}, nil, nil},
		{[][]uint32{{0}}, map[uint32]string{}, map[uint32]string{}, nil, nil},
		{[][]uint32{{0}}, map[uint32]string{0: "a"}, map[uint32]string{}, []string{"a"}, nil},
		{[][]uint32{{0}}, map[uint32]string{}, map[uint32]string{0: "A"}, nil, []string{"A"}},
		{[][]uint32{{0, 1, 2}}, map[uint32]string{0: "a", 1: "b", 2: "c"}, map[uint32]string{}, []string{"a"}, nil},
		{[][]uint32{{0, 1, 2}, {1}}, map[uint32]string{0: "a", 1: "b", 2: "c"}, map[uint32]string{}, []string{"b"}, nil},
		{[][]uint32{{0, 1, 2}, {1, 2}}, map[uint32]string{0: "a", 1: "b", 2: "c"}, map[uint32]string{}, []string{"b"}, nil},
		{[][]uint32{{0, 1}, {2}}, map[uint32]string{0: "a", 1: "b", 2: "c"}, map[uint32]string{}, []string{"a", "c"}, nil},
		{[][]uint32{{0, 1}, {2}, {1}}, map[uint32]string{0: "a", 1: "b"}, map[uint32]string{2: "C"}, []string{"b"}, []string{"C"}},

		// At least one trace with no literal: don't optimize
		{[][]uint32{{0, 1}, {2}, {1}, {42}}, map[uint32]string{0: "a", 1: "b"}, map[uint32]string{2: "C"}, nil, nil},
		{[][]uint32{{0, 1}, {2}, {1}, {}}, map[uint32]string{0: "a", 1: "b"}, map[uint32]string{2: "C"}, nil, nil},
	}

	for _, test := range tests {
		lits, foldedLits := rankLiterals(test.traces, test.nofolds, test.folds)

		require.Equal(t, test.expectedLits, lits, test)
		require.Equal(t, test.expectedFoldedLits, foldedLits, test)
	}
}

func TestPickBestLiteral(t *testing.T) {
	tests := []struct {
		pcs              []uint32
		nofolds          map[uint32]string
		folds            map[uint32]string
		expectedPc       uint32
		expectedLit      string
		expectedFoldCase bool
	}{
		{[]uint32{}, map[uint32]string{}, map[uint32]string{}, 0, "", false},
		{[]uint32{1}, map[uint32]string{1: "a"}, map[uint32]string{}, 1, "a", false},
		{[]uint32{2}, map[uint32]string{}, map[uint32]string{2: "B"}, 2, "B", true},
		{[]uint32{2}, map[uint32]string{1: "a"}, map[uint32]string{2: "B"}, 2, "B", true},
		{[]uint32{1, 2}, map[uint32]string{1: "a"}, map[uint32]string{2: "B"}, 1, "a", false},
		{[]uint32{1, 2}, map[uint32]string{1: "a", 3: "abc"}, map[uint32]string{2: "BC"}, 2, "BC", true},
	}

	for _, test := range tests {
		pc, lit, fc := pickBestLiteral(test.pcs, test.nofolds, test.folds)
		require.Equal(t, test.expectedPc, pc, test)
		require.Equal(t, test.expectedLit, lit, test)
		require.Equal(t, test.expectedFoldCase, fc, test)
	}
}

func TestMaxFrequency(t *testing.T) {
	freq := map[uint32][]int{
		0: {0},
		1: {0, 0},
		2: {0, 0},
	}

	actual := maxFrequency(freq) // Random order

	require.Len(t, actual, 2)
	require.Contains(t, actual, uint32(1))
	require.Contains(t, actual, uint32(2))
}

func TestComputeFrequencies(t *testing.T) {
	traces := [][]uint32{
		{0, 1},
		{1, 2},
		{3},
	}

	nofolds := map[uint32]string{0: "a", 1: "b"}
	folds := map[uint32]string{3: "C"}

	actual := computeFrequencies(traces, nofolds, folds)

	require.Equal(
		t,
		map[uint32][]int{
			0: {0},
			1: {0, 1},
			3: {2},
		},
		actual)
}

func TestSimulateTraces(t *testing.T) {
	prog := &syntax.Prog{
		Start: 1,
		Inst: []syntax.Inst{
			{Op: syntax.InstMatch},
			{Op: syntax.InstAlt, Arg: 2, Out: 4},

			{Op: syntax.InstRune1, Rune: []rune{'a'}, Out: 3},
			{Op: syntax.InstAlt, Arg: 2, Out: 0},

			{Op: syntax.InstRune1, Rune: []rune{'b'}, Out: 0},
		},
	}

	traces, ok := simulateTraces(prog, extractBranchTargets(prog))

	require.Equal(t, [][]uint32{{1, 4, 0}, {1, 2, 0}}, traces)
	require.True(t, ok)
}

func TestSimulateTracesPathExplosion(t *testing.T) {
	prog, err := compile("[a-f]{64}")
	require.NoError(t, err)

	// Test successful unroll
	alts := 0
	for _, inst := range prog.Inst {
		if inst.Op == syntax.InstAlt {
			alts++
		}
	}
	require.Greater(t, alts, 64)

	for i := 0; i < 50; i++ {
		_, ok := simulateTraces(prog, extractBranchTargets(prog))
		require.False(t, ok)
	}
}

func TestExtractTargetLiterals(t *testing.T) {
	prog := &syntax.Prog{
		Inst: []syntax.Inst{
			{Op: syntax.InstMatch},
			{Op: syntax.InstAlt, Arg: 2, Out: 6},

			{Op: syntax.InstRune1, Rune: []rune{'a'}, Out: 3},
			{Op: syntax.InstRuneAny, Out: 4},
			{Op: syntax.InstRune1, Rune: []rune{'b'}, Out: 5},
			{Op: syntax.InstRune, Rune: []rune{'C'}, Arg: uint32(syntax.FoldCase), Out: 0},

			{Op: syntax.InstRune1, Rune: []rune{'d'}, Out: 0},
		},
	}

	nofolds, folds := extractTargetLiterals(prog)

	require.Equal(t, map[uint32]string{6: "d"}, nofolds)
	require.Equal(t, map[uint32]string{2: "BC"}, folds)
}

func TestExtractBranchTargets(t *testing.T) {
	prog := &syntax.Prog{
		Start: 42,
		Inst: []syntax.Inst{
			{Op: syntax.InstRune1, Out: 2},
			{Op: syntax.InstAlt, Arg: 3, Out: 5},
			{Op: syntax.InstAltMatch, Arg: 7, Out: 11},
			{Op: syntax.InstMatch, Arg: 13, Out: 17},
			{Op: syntax.InstRune1, Out: 21},
			{Op: syntax.InstRune1, Out: 21},
		},
	}

	expected := map[uint32]bool{
		42: true,
		3:  true,
		5:  true,
		7:  true,
		11: true,
		21: true,
	}
	actual := extractBranchTargets(prog)
	require.Equal(t, expected, actual)
}

func TestExtractBranchBestLiteral(t *testing.T) {
	tests := []struct {
		regex            string
		expectedLit      string
		expectedFoldCase bool
	}{
		{"abc", "abc", false},
		{"a.b(?i)c", "BC", true},
		{"ab.(?i)c", "ab", false},
		{"a(?:b)?", "a", false},
		{"a.bc", "bc", false},
	}

	for _, test := range tests {
		prog, err := compile(test.regex)
		require.NoError(t, err, test)

		lit, fc := extractBranchBestLiteral(prog, uint32(prog.Start))
		require.Equal(t, test.expectedLit, lit, test)
		require.Equal(t, test.expectedFoldCase, fc, test)
	}
}

func TestIsBetterLiteral(t *testing.T) {
	tests := []struct {
		lit1           string
		fc1            bool
		lit2           string
		fc2            bool
		expectedResult bool
	}{
		{"", false, "", false, false},
		{"", false, "a", false, true},
		{"a", false, "a", false, false},
		{"a", false, "b", false, false},
		{"b", false, "a", false, true},
		{"a", false, "bb", false, true},

		// Case folding
		{"a", true, "b", false, true},
		{"a", true, "b", true, false},
		{"b", true, "a", true, true},
		{"a", false, "bb", true, true},

		// Runes, not bytes
		{"€", false, "a", false, true},
	}

	for _, test := range tests {
		result := isBetterLiteral(test.lit1, test.fc1, test.lit2, test.fc2)
		require.Equal(t, test.expectedResult, result, test)
	}
}

func TestExtractLiteral(t *testing.T) {
	tests := []struct {
		regex            string
		expectedLit      string
		expectedFoldCase bool
		ignorePc         bool
	}{
		{"abc", "abc", false, false},
		{"ab(?i)c", "ABC", true, false},
		{"a(?:b)?", "a", false, true},
	}

	for _, test := range tests {
		prog, err := compile(test.regex)
		require.NoError(t, err, test)

		lit, fc, newPc := extractLiteral(prog, uint32(prog.Start))
		require.Equal(t, test.expectedLit, lit, test)
		require.Equal(t, test.expectedFoldCase, fc, test)
		if !test.ignorePc {
			require.Equal(t, newPc, uint32(prog.Start+len(lit)), prog, test)
		}
	}
}

func TestExtractRune(t *testing.T) {
	tests := []struct {
		inst             syntax.Inst
		expectedOk       bool
		expectedRune     rune
		expectedFoldCase bool
	}{
		// InstRune1
		{syntax.Inst{Op: syntax.InstRune1, Rune: []rune{'a'}}, true, 'a', false},
		{syntax.Inst{Op: syntax.InstRune1, Rune: []rune{'€'}}, false, '€', false},

		// InstRune
		{syntax.Inst{Op: syntax.InstRune, Rune: []rune{'b'}}, true, 'b', false},
		{syntax.Inst{Op: syntax.InstRune, Rune: []rune{'C'}, Arg: uint32(syntax.FoldCase)}, true, 'C', true},
		{syntax.Inst{Op: syntax.InstRune, Rune: []rune{'A', 'F'}, Arg: uint32(syntax.FoldCase)}, false, 0, false},
		{syntax.Inst{Op: syntax.InstRune, Rune: []rune{'€'}}, false, '€', false},

		// Irrelevant instructions
		{syntax.Inst{Op: syntax.InstRuneAny}, false, 0, false},
		{syntax.Inst{Op: syntax.InstMatch}, false, 0, false},
		{syntax.Inst{Op: syntax.InstAlt}, false, 0, false},
	}

	for _, test := range tests {
		ok, r, fc := extractRune(test.inst)
		require.Equal(t, test.expectedOk, ok, test)
		require.Equal(t, test.expectedRune, r, test)
		require.Equal(t, test.expectedFoldCase, fc, test)
	}
}
