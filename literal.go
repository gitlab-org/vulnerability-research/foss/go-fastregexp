package fastregexp

import (
	"regexp/syntax"
	"strings"
	"unicode"

	"github.com/pkg/errors"
)

// maxSimulatedPaths determines how many paths we will attempt to simulate when walking
// through a regex
const maxSimulatedPaths = 1024

var (
	branchInstructions map[syntax.InstOp]bool = map[syntax.InstOp]bool{
		syntax.InstAlt:      true,
		syntax.InstAltMatch: true,
	}

	nonBranchInstructions map[syntax.InstOp]bool = map[syntax.InstOp]bool{
		syntax.InstCapture:      true,
		syntax.InstEmptyWidth:   true,
		syntax.InstNop:          true,
		syntax.InstRune:         true,
		syntax.InstRune1:        true,
		syntax.InstRuneAny:      true,
		syntax.InstRuneAnyNotNL: true,
	}

	terminatingInstructions map[syntax.InstOp]bool = map[syntax.InstOp]bool{
		syntax.InstMatch: true,
		syntax.InstFail:  true,
	}
)

// FindLiterals from the provided regex both in literal form and case folded literal form.
// Returns error if unable to compile, or nil if unable to extract literals.
func FindLiterals(regex string) (lits []string, foldedLits []string, err error) {
	prog, err := compile(regex)
	if err != nil {
		return nil, nil, errors.Wrapf(err, "unable to preprocess: %s", regex)
	}

	nofolds, folds := extractTargetLiterals(prog)
	traces, ok := simulateTraces(prog, extractBranchTargets(prog))
	if !ok {
		// Cannot optimize
		return nil, nil, nil
	}

	lits, foldedLits = rankLiterals(traces, nofolds, folds)

	return lits, foldedLits, nil
}

// compile the regex using Perl syntax, returning a compiled regular expression
// program.
func compile(regex string) (*syntax.Prog, error) {
	re, err := syntax.Parse(regex, syntax.Perl)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to parse: %s", regex)
	}

	prog, err := syntax.Compile(re.Simplify())
	if err != nil {
		return nil, errors.Wrapf(err, "unable to compile: %s", regex)
	}

	prog = unrollRuneRanges(prog)

	return prog, nil
}

// unrollRuneRanges returns a new regex program in which InstRune rune ranges
// instructions are rewritten as InstAlt and InstRune1 instructions. this function
// internally handles remapping instruction flow, thereby making the original
// InstRune instructions unreachable.
func unrollRuneRanges(prog *syntax.Prog) *syntax.Prog {
	// is this copy necessary? We overwrite the prog on assignment anyways
	// in compile
	newInsts := make([]syntax.Inst, len(prog.Inst))
	copy(newInsts, prog.Inst)

	// remaps the instructions indexes after injection
	remapProgramCounterIndex := make(map[uint32]uint32)

	// iterate over each inst in the program, rewriting all InstRune
	// rune ranges into InstAlt branching and InstRune1 rune matching
	// instruction and saves the newly emitted program counter corresponding
	// to the unrolled InstRune one.
	for pc, inst := range prog.Inst {
		runes := extractRunesToUnroll(inst)
		if len(runes) == 0 {
			continue
		}

		newPc := uint32(len(newInsts))
		newInsts = append(newInsts, emitUnrolledInstructions(newPc, inst.Out, runes)...)
		remapProgramCounterIndex[uint32(pc)] = newPc
	}

	// Remap all instructions of the new program
	for i := 0; i < len(newInsts); i++ {
		inst := &newInsts[i]

		// Remap instruction's Out property
		if branchInstructions[inst.Op] || nonBranchInstructions[inst.Op] {
			if newPc, ok := remapProgramCounterIndex[uint32(inst.Out)]; ok {
				inst.Out = newPc
			}
		}

		// Remap instruction's Arg property
		if branchInstructions[inst.Op] {
			if newPc, ok := remapProgramCounterIndex[uint32(inst.Arg)]; ok {
				inst.Arg = newPc
			}
		}
	}

	// Remap program Start property
	start := prog.Start
	if newPc, ok := remapProgramCounterIndex[uint32(prog.Start)]; ok {
		start = int(newPc)
	}

	return &syntax.Prog{
		Inst:   newInsts,
		Start:  start,
		NumCap: prog.NumCap,
	}
}

// emitUnrolledInstructions generates a series of InstAlts, the last two runes share
// the same InstAlt. Also adds matching InstRune1 (containing the rune value).
func emitUnrolledInstructions(counter uint32, out uint32, runes []rune) []syntax.Inst {
	insts := make([]syntax.Inst, 0, 2*len(runes))

	// Create 1 InstAlt per rune except for last rune
	for i := 0; i < len(runes)-1; i++ {
		insts = append(insts, syntax.Inst{
			Op:  syntax.InstAlt,
			Out: counter + uint32(1+i),
			Arg: counter + uint32(2*len(runes)-2-i),
		})
	}

	// Create one InstRune1 per rune
	for _, r := range runes {
		insts = append(insts, syntax.Inst{
			Op:   syntax.InstRune1,
			Out:  out,
			Rune: []rune{r},
		})
	}

	return insts
}

// extractRunesToUnroll from the Inst. Accumulates a slice
// of all runes for this instruction, or returns nil if
// it should not unroll the runes. (// deals with ranges)
func extractRunesToUnroll(inst syntax.Inst) []rune {
	if !shouldUnroll(inst) {
		return nil
	}

	runes := make([]rune, 0, 8)
	for i := 0; i < len(inst.Rune); i += 2 {
		for r := inst.Rune[i]; r <= inst.Rune[i+1]; r++ {
			runes = append(runes, r)
		}
	}

	return runes
}

// shouldUnroll returns true if the instruction is a InstRune,
// it has a length of 2, 4, 6, or 8. This is for character ranges
// and we limit up to 8 as otherwise there is a potential for
// exponential explosion of paths/branches to take.
func shouldUnroll(inst syntax.Inst) bool {
	if inst.Op != syntax.InstRune {
		return false
	}

	// example: [a-z] = 2, [a-zA-Z] = 4 etc.
	switch len(inst.Rune) {
	case 2, 4, 6, 8:
		break
	default:
		return false
	}

	var count int64
	for i := 0; i < len(inst.Rune); i += 2 {
		count += int64(inst.Rune[i+1]-inst.Rune[i]) + 1
	}

	return count <= 8
}

func rankLiterals(traces [][]uint32, nofolds map[uint32]string, folds map[uint32]string) (literals []string, foldedLits []string) {
	tracesToCover := make([][]uint32, len(traces))
	copy(tracesToCover, traces)

	for len(tracesToCover) > 0 {
		freq := computeFrequencies(tracesToCover, nofolds, folds)
		if len(freq) == 0 {
			return nil, nil
		}

		pcs := maxFrequency(freq)
		pc, literal, foldCase := pickBestLiteral(pcs, nofolds, folds)

		if foldCase {
			foldedLits = append(foldedLits, literal)
		} else {
			literals = append(literals, literal)
		}

		nextTracesToCover := tracesToCover[:0]
		// creates the next traces to cover, without the ones covered by the literal picked above
		for i := 0; i < len(tracesToCover); i++ {
			found := false
			for _, j := range freq[pc] {
				if i == j {
					found = true
					break
				}
			}
			if !found {
				nextTracesToCover = append(nextTracesToCover, tracesToCover[i])
			}
		}
		tracesToCover = nextTracesToCover
	}

	return literals, foldedLits
}

func pickBestLiteral(pcs []uint32, nofolds map[uint32]string, folds map[uint32]string) (pickedPc uint32, literal string, foldCase bool) {
	for _, pc := range pcs {
		if noFoldedLiteral, ok := nofolds[pc]; ok {
			if isBetterLiteral(literal, foldCase, noFoldedLiteral, false) {
				pickedPc = pc
				literal = noFoldedLiteral
				foldCase = false
			}
		}

		if foldedLiteral, ok := folds[pc]; ok {
			if isBetterLiteral(literal, foldCase, foldedLiteral, true) {
				pickedPc = pc
				literal = foldedLiteral
				foldCase = true
			}
		}
	}

	return pickedPc, literal, foldCase
}

func maxFrequency(freq map[uint32][]int) []uint32 {
	var result []uint32
	max := 0

	for pc, traces := range freq {
		if len(traces) > max {
			max = len(traces)
			result = result[:0]
			result = append(result, pc)
		} else if len(traces) == max {
			result = append(result, pc)
		}
	}

	return result
}

// computeFrequencies of the program counters with associated literals
func computeFrequencies(traces [][]uint32, nofolds map[uint32]string, folds map[uint32]string) map[uint32][]int {
	freq := make(map[uint32][]int)

	for i, trace := range traces {
		found := false

		for _, pc := range trace {
			_, hasNoFoldedLiteral := nofolds[pc]
			_, hasFoldedLiteral := folds[pc]

			if hasNoFoldedLiteral || hasFoldedLiteral {
				freq[pc] = append(freq[pc], i)
				found = true
			}
		}

		if !found {
			return nil
		}
	}

	return freq
}

// simulateTraces walks through the program and it's branches
func simulateTraces(prog *syntax.Prog, tracedPcs map[uint32]bool) (traces [][]uint32, ok bool) {
	var workSet [][]uint32

	workSet = append(workSet, []uint32{uint32(prog.Start)})
	for len(workSet) > 0 && len(workSet) < maxSimulatedPaths && len(traces) < maxSimulatedPaths {
		trace := workSet[0]
		workSet = workSet[1:] // pop

		pc := trace[len(trace)-1]
		trace = trace[:len(trace)-1] // resumes
		visited := make(map[uint32]bool)
		for _, prevPc := range trace {
			visited[prevPc] = true
		}

		for !visited[pc] {
			visited[pc] = true

			if tracedPcs[pc] {
				trace = append(trace, pc)
			}

			inst := prog.Inst[pc]
			if terminatingInstructions[inst.Op] {
				traces = append(traces, trace)
			} else if branchInstructions[inst.Op] {
				if !visited[inst.Arg] {
					newTrace := make([]uint32, len(trace)+1)
					copy(newTrace, trace)
					newTrace[len(newTrace)-1] = inst.Arg

					workSet = append(workSet, newTrace)
				}
				if !visited[inst.Out] && inst.Arg != inst.Out {
					newTrace := make([]uint32, len(trace)+1)
					copy(newTrace, trace)
					newTrace[len(newTrace)-1] = inst.Out

					workSet = append(workSet, newTrace)
				}
			} else {
				pc = inst.Out
			}
		}
	}

	if len(workSet) >= maxSimulatedPaths || len(traces) >= maxSimulatedPaths {
		return nil, false
	}

	return traces, true
}

// extractTargetLiterals for the program, gathers the best literal for each branch
// of the regex program. Care is taken to track case-folded/non-case-folded literals
// (e.g. upper/lower case).
func extractTargetLiterals(prog *syntax.Prog) (nofolds map[uint32]string, folds map[uint32]string) {
	nofolds = make(map[uint32]string)
	folds = map[uint32]string{}

	// iterate over branches
	for pc := range extractBranchTargets(prog) {
		// for each branch extract the best literal (longer literal == better, etc)
		literal, foldCase := extractBranchBestLiteral(prog, pc)
		if literal != "" {
			if foldCase {
				folds[pc] = literal
			} else {
				nofolds[pc] = literal
			}
		}
	}

	return nofolds, folds
}

// extractBranchTargets iterates over our program, tracking any
// branch instructions, their args, and where they point to.
func extractBranchTargets(prog *syntax.Prog) map[uint32]bool {
	targets := make(map[uint32]bool)
	joins := make(map[uint32]int)

	targets[uint32(prog.Start)] = true
	for _, inst := range prog.Inst {
		if branchInstructions[inst.Op] {
			targets[inst.Arg] = true
			targets[inst.Out] = true
		} else {
			joins[inst.Out]++
		}
	}

	for pc, count := range joins {
		// needs more than 1 to be a proper join
		if count > 1 {
			targets[pc] = true
		}
	}

	return targets
}

// extractBranchBestLiteral for this branch of the program.
func extractBranchBestLiteral(prog *syntax.Prog, pc uint32) (literal string, foldCase bool) {
	visited := make(map[uint32]bool)
	for !visited[pc] {
		visited[pc] = true

		extractedLiteral, extractedFoldCase, newPc := extractLiteral(prog, pc)
		if isBetterLiteral(literal, foldCase, extractedLiteral, extractedFoldCase) {
			literal = extractedLiteral
			foldCase = extractedFoldCase
		}

		pc = newPc
		inst := prog.Inst[pc]

		// stop upon reaching the first branching/terminating instruction
		if !nonBranchInstructions[inst.Op] {
			break
		}
		pc = inst.Out
	}

	return literal, foldCase
}

// isBetterLiteral determines which literal is better by looking at length,
// then whether or not the case is folded. If they're both folded and the same
// length, then use the sort order of the literal to determine which is better.
func isBetterLiteral(literal1 string, foldCase1 bool, literal2 string, foldCase2 bool) bool {
	literal1Len := len([]rune(literal1))
	literal2Len := len([]rune(literal2))

	if literal2Len > literal1Len {
		return true
	} else if literal1Len == literal2Len {
		if foldCase1 && !foldCase2 {
			return true
		} else if foldCase1 == foldCase2 && literal2 < literal1 {
			return true
		}
	}

	return false
}

// extractLiteral by writing runes into a string for an instruction, updating the program
// counter to the final instructions inst.Out value.
func extractLiteral(prog *syntax.Prog, pc uint32) (literal string, foldCase bool, newPc uint32) {
	var sb strings.Builder

	visited := make(map[uint32]bool)
	for !visited[pc] {
		visited[pc] = true

		inst := prog.Inst[pc]
		ok, extractedRune, wasFoldCased := extractRune(inst)
		if !ok {
			break
		}
		sb.WriteRune(extractedRune)
		foldCase = foldCase || wasFoldCased
		pc = inst.Out
	}

	literal = sb.String()
	if foldCase {
		literal = strings.ToUpper(literal)
	}

	return literal, foldCase, pc
}

// extractRune from an instruction, returning true if it's a valid rune.
// Returning true for foldCase if we must uppercase the rune
func extractRune(inst syntax.Inst) (ok bool, extractedRune rune, foldCase bool) {
	if inst.Op == syntax.InstRune1 && len(inst.Rune) == 1 {
		extractedRune = inst.Rune[0]
		foldCase = false
	} else if inst.Op == syntax.InstRune && len(inst.Rune) == 1 {
		extractedRune = inst.Rune[0]
		if syntax.Flags(inst.Arg)&syntax.FoldCase != 0 {
			foldCase = true
		}
	} else {
		return false, extractedRune, foldCase
	}

	if extractedRune > unicode.MaxASCII {
		return false, extractedRune, foldCase
	}

	return true, extractedRune, foldCase
}
